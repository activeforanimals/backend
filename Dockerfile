# Using https://github.com/docker-library/drupal
# from https://www.drupal.org/docs/8/system-requirements/drupal-8-php-requirements
FROM php:7.2-apache

ENV COMPOSER_SIGNATURE=544e09ee996cdf60ece3804abc52599c22b1f40f4323403c44d44fdfdd586475ca9813a858088ffbc1f233e9b180f061

# install the PHP extensions we need
RUN set -ex; \
	\
	if command -v a2enmod; then \
		a2enmod rewrite; \
	fi; \
	\
	savedAptMark="$(apt-mark showmanual)"; \
	\
	apt-get update; \
	apt-get install -y --no-install-recommends \
    libjpeg-dev \
		libpng-dev \
		libpq-dev \
	; \
	\
	docker-php-ext-configure gd --with-png-dir=/usr --with-jpeg-dir=/usr; \
	docker-php-ext-install -j "$(nproc)" \
		gd \
		opcache \
		pdo_mysql \
		pdo_pgsql \
		zip \
	; \
	\
# reset apt-mark's "manual" list so that "purge --auto-remove" will remove all build dependencies
	apt-mark auto '.*' > /dev/null; \
	apt-mark manual $savedAptMark; \
	ldd "$(php -r 'echo ini_get("extension_dir");')"/*.so \
		| awk '/=>/ { print $3 }' \
		| sort -u \
		| xargs -r dpkg-query -S \
		| cut -d: -f1 \
		| sort -u \
		| xargs -rt apt-mark manual; \
	\
	apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false; \
# install the tools we need after autoremoval
  apt-get install -y --no-install-recommends \
    git \
    mysql-client \
    sudo \
    unzip \
  ; \
  \
# clear lists
	rm -rf /var/lib/apt/lists/*

# set recommended PHP.ini settings
# see https://secure.php.net/manual/en/opcache.installation.php
RUN { \
		echo 'opcache.memory_consumption=128'; \
		echo 'opcache.interned_strings_buffer=8'; \
		echo 'opcache.max_accelerated_files=4000'; \
		echo 'opcache.revalidate_freq=60'; \
		echo 'opcache.fast_shutdown=1'; \
		echo 'opcache.enable_cli=1'; \
	} > /usr/local/etc/php/conf.d/opcache-recommended.ini

# install composer
RUN \
    php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"; \
    php -r "if (hash_file('SHA384', 'composer-setup.php') === '${COMPOSER_SIGNATURE}') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"; \
    php composer-setup.php --install-dir=/usr/local/bin --filename=composer; \
    php -r "unlink('composer-setup.php');";

# Add files
ADD data/drupal/ /var/www/html/
ADD data/composer.json /var/www/html/
ADD data/autoload.php /var/www/html/vendor/drupal/

RUN \
    chown -R www-data:www-data /var/www/html/sites; \
    chown -R www-data:www-data /var/www/html/vendor;

WORKDIR /var/www/html

# Run composer install and add symbolic links required for Drupal to work
RUN \
    sudo -u www-data composer install --no-dev; \
    ln -s /var/www/html/vendor/drupal/core /var/www/html/core; \
    ln -s /var/www/html/sites /var/www/html/vendor/drupal/sites; \
    ln -s /var/www/html/vendor/drupal/modules /var/www/html/modules; \
    ln -s /var/www/html/vendor/drupal/themes /var/www/html/themes; \
    ln -s /var/www/html/vendor/drupal/profiles /var/www/html/profiles;

# Remove access to vendor folder for www-data user.
RUN \
    chown -R root /var/www/html/vendor;
